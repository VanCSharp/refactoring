﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ItDepends
{
    internal static class Dependency
    {
        public static IEnumerable<T> DependencyOrder<T>(Func<T, IEnumerable<T>> tDeps, T target)
        {
            return DependencyOrderInner(tDeps, target, new HashSet<T>());
        }

        private static IEnumerable<T> DependencyOrderInner<T>(Func<T, IEnumerable<T>> tDeps, T target, ISet<T> seen)
        {
            if (seen.Contains(target))
            {
                yield break;
            }
            
            seen.Add(target);
            
            foreach (var tr in tDeps(target))
            {
                foreach (var dep in DependencyOrderInner(tDeps, tr, seen))
                {
                    yield return dep;
                }
            }
            yield return target;
        }
    }
}