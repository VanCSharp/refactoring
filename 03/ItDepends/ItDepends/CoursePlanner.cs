﻿using System.Collections.Generic;
using System.Linq;

namespace ItDepends
{
    internal static class CoursePlanner
    {
        private static void Plan(IReadOnlyDictionary<string, Course> courses, string targetCourse, ICollection<string> plan)
        {
            foreach (var prerequisite in courses[targetCourse].Prerequisites)
            {
                Plan(courses, prerequisite, plan);
            }
            plan.Add(targetCourse);
        }

        public static IEnumerable<string> Plan(Dictionary<string, Course> courses, string targetCourse)
        {
            var plan = new List<string>();
            Plan(courses, targetCourse, plan);
            return plan.Distinct();
        }
    }
}