﻿using System;
using System.Collections.Generic;

namespace ItDepends
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            var courses = new Dictionary<string, string[]>
            {
                {
                    "math402",
                    new[]
                    {
                        "math300",
                        "math301"
                    }
                },
                {
                    "math300",
                    new[]
                    {
                        "math200"
                    }
                },
                {
                    "math301",
                    new[]
                    {
                        "math201"
                    }
                },
                {
                    "math200",
                    new[]
                    {
                        "math100",
                        "math101"
                    }
                },
                {
                    "math201",
                    new[]
                    {
                        "math100",
                        "math101"
                    }
                },
                {
                    "math100",
                    new string[]
                    {
                    }
                },
                {
                    "math101",
                    new string[]
                    {
                    }
                },
            };
            
            foreach (var course in Dependency.DependencyOrder(x=>courses[x], "math402"))
            {
                Console.WriteLine(course);
            }
        }
    }
}