﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ItDepends
{
    internal static class Dependency
    {
        public static IEnumerable<T> DependencyOrder<T>(Func<T, IEnumerable<T>> tDeps, T target)
        {
            return DependencyOrderInner(tDeps, target).Distinct();
        }

        private static IEnumerable<T> DependencyOrderInner<T>(Func<T, IEnumerable<T>> tDeps, T target)
        {
            foreach (var tr in tDeps(target))
            {
                foreach (var dep in DependencyOrderInner(tDeps, tr))
                {
                    yield return dep;
                }
            }
            yield return target;
        }
    }
}