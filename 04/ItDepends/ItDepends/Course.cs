﻿using System.Collections.Generic;

namespace ItDepends
{
    internal sealed class Course
    {
        public Course(string name, IEnumerable<string> prerequisites)
        {
            Prerequisites = prerequisites;
            Name = name;
        }

        public IEnumerable<string> Prerequisites { get; }
        public string Name { get; }
    }
}