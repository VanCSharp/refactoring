﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ItDepends
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            var courses = new Dictionary<string, Course>();
            foreach (var e in new[]
            {
                new {n = "math402", r = new[] {"math300", "math301"}},
                new {n = "math300", r = new[] {"math200"}},
                new {n = "math301", r = new[] {"math201"}},
                new {n = "math200", r = new[] {"math100", "math101"}},
                new {n = "math201", r = new[] {"math100", "math101"}},
                new {n = "math100", r = new string[] { }},
                new {n = "math101", r = new string[] { }},
            })
            {
                courses[e.n] = new Course(e.n, e.r);
            }

            foreach (var course in Course.Plan(courses, "math402"))
            {
                Console.WriteLine(course);
            }
        }
    }
}