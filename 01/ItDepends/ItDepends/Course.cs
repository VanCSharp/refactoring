﻿using System.Collections.Generic;
using System.Linq;

namespace ItDepends
{
    internal sealed class Course
    {
        static internal void Plan(Dictionary<string, Course> courses, string targetCourse, List<string> plan)
        {
            foreach (var prerequisite in courses[targetCourse].Prerequisites)
            {
                Plan(courses, prerequisite, plan);
            }
            plan.Add(targetCourse);
        }

        static public IEnumerable<string> Plan(Dictionary<string, Course> courses, string targetCourse)
        {
            var plan = new List<string>();
            Plan(courses, targetCourse, plan);
            return plan.Distinct();
        }

        public Course(string name, IEnumerable<string> prerequisites)
        {
            Prerequisites = prerequisites;
            Name = name;
        }

        public IEnumerable<string> Prerequisites { get; }
        public string Name { get; }
    }
}